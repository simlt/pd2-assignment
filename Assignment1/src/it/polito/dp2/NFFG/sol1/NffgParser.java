package it.polito.dp2.NFFG.sol1;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import it.polito.dp2.NFFG.LinkReader;
import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.NodeReader;
import it.polito.dp2.NFFG.sol1.jaxb.*;

public class NffgParser implements NffgReader {
	private Nffg nffg;
	private Map<String, NodeParser> nodeMap;

	public NffgParser(Nffg nffg) {
		this.nffg = nffg;
		// Create nodes
		nodeMap = nffg.getNode().parallelStream().collect(Collectors.toMap(Node::getName, NodeParser::new));
		// After creation of nodes we can now reference them when reading links
		for (Node n : nffg.getNode()) {
			Set<LinkReader> links = n.getLink().parallelStream()
					.map(l -> new LinkParser(l, getNode(n.getName()), getNode(l.getDestination())))
					.collect(Collectors.toSet());
			// Now add the links to the node
			nodeMap.get(n.getName()).setLink(links);
		}
	}

	@Override
	public String getName() {
		return nffg.getName();
	}

	@Override
	public NodeReader getNode(String arg0) {
		return nodeMap.get(arg0);
	}

	@Override
	public Set<NodeReader> getNodes() {
		return new HashSet<NodeReader>(nodeMap.values());
	}

	@Override
	public Calendar getUpdateTime() {
		return nffg.getLastUpdate().toGregorianCalendar();
	}

}
