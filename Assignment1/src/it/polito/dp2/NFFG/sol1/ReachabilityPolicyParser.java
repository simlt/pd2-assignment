package it.polito.dp2.NFFG.sol1;

import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.NodeReader;
import it.polito.dp2.NFFG.ReachabilityPolicyReader;
import it.polito.dp2.NFFG.sol1.jaxb.*;

public class ReachabilityPolicyParser extends PolicyParser implements ReachabilityPolicyReader {
	NodeReader source;
	NodeReader destination;

	public ReachabilityPolicyParser(Policy policy, NffgReader nffg) {
		super(policy, nffg);
		source = nffg.getNode(policy.getReachability().getSource());
		destination = nffg.getNode(policy.getReachability().getDestination());
	}

	@Override
	public NodeReader getDestinationNode() {
		return destination;
	}

	@Override
	public NodeReader getSourceNode() {
		return source;
	}
}
