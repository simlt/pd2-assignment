package it.polito.dp2.NFFG.sol1;

import java.util.Set;
import java.util.stream.Collectors;

import it.polito.dp2.NFFG.FunctionalType;
import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.TraversalPolicyReader;
import it.polito.dp2.NFFG.sol1.jaxb.Policy;

public class TraversalPolicyParser extends ReachabilityPolicyParser implements TraversalPolicyReader {
	Set<FunctionalType> functionalTypes;

	public TraversalPolicyParser(Policy policy, NffgReader nffg) {
		super(policy, nffg);
		functionalTypes = policy.getTraversal().stream().map(t -> FunctionalType.valueOf(t.getType().value()))
				.collect(Collectors.toSet());
	}

	@Override
	public Set<FunctionalType> getTraversedFuctionalTypes() {
		return functionalTypes;
	}
}
