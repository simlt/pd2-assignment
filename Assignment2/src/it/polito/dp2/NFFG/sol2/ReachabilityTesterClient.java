package it.polito.dp2.NFFG.sol2;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.StatusType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;

import it.polito.dp2.NFFG.FactoryConfigurationError;
import it.polito.dp2.NFFG.LinkReader;
import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.NffgVerifier;
import it.polito.dp2.NFFG.NffgVerifierException;
import it.polito.dp2.NFFG.NffgVerifierFactory;
import it.polito.dp2.NFFG.NodeReader;
import it.polito.dp2.NFFG.lab2.NoGraphException;
import it.polito.dp2.NFFG.lab2.ReachabilityTester;
import it.polito.dp2.NFFG.lab2.ServiceException;
import it.polito.dp2.NFFG.lab2.UnknownNameException;
import it.polito.dp2.NFFG.sol2.Localhost_Neo4JXMLRest.Resource;
import it.polito.dp2.NFFG.sol2.Localhost_Neo4JXMLRest.Resource.NodeNodeidRelationship;

public class ReachabilityTesterClient implements ReachabilityTester {
	private NffgVerifier nffgVerifier;
	private Resource resource;
	private String currentNffg;
	/** Used to cache locally node ids needed to create node relationships. Key: NodeName, value: NodeID on NEO4J */
	private Map<String, String> nodeIds = new HashMap<>();

	/** Number of tries for a network operation */
	private static final int NETWORK_TRIES = 3;

	public ReachabilityTesterClient() throws NffgVerifierException, FactoryConfigurationError, URISyntaxException {
		this.currentNffg = null;
		this.nffgVerifier = NffgVerifierFactory.newInstance().newNffgVerifier();
		// Read URL property
		String URL = System.getProperty("it.polito.dp2.NFFG.lab2.URL");
		if (URL != null) {
			Client client = Localhost_Neo4JXMLRest.createClient();
			this.resource = Localhost_Neo4JXMLRest.resource(client, new URI(URL));
		} else {
			this.resource = Localhost_Neo4JXMLRest.resource();
		}
	}

	@Override
	public void loadNFFG(String name) throws UnknownNameException, ServiceException {
		try {
			// Check name first in order to avoid any server modification in
			// case of exception
			NffgReader nffg = nffgVerifier.getNffg(name);
			if (nffg == null)
				throw new UnknownNameException("Could not find the requested NFFG");

			// Set currently loaded graph to null, so if any error occurs it
			// remains invalid
			currentNffg = null;

			// Cleanup Neo4J DB and local cache
			nodeIds.clear();
			tryNetworkOperation(() -> resource.nodes().deleteAsXml(ClientResponse.class), Status.OK);
			// Export new nffg to DB
			Property property = new Property();
			Node node = new Node();
			Relationship relationship = new Relationship();
			property.setName("name");
			relationship.setType("Link");

			for (NodeReader nodeReader : nffg.getNodes()) {
				property.setValue(nodeReader.getName());
				node.getProperty().add(property);
				Node nodeResponse = tryNetworkOperation(() -> resource.node().postXml(node, ClientResponse.class),
						Status.OK, Node.class);
				// Cache node id from response for later use
				nodeIds.put(nodeReader.getName(), nodeResponse.getId());
			}

			// Create links after all nodes are loaded
			for (NodeReader nodeReader : nffg.getNodes()) {
				NodeNodeidRelationship nodeRelationship = resource
						.nodeNodeidRelationship(nodeIds.get(nodeReader.getName()));
				for (LinkReader linkReader : nodeReader.getLinks()) {
					String destId = nodeIds.get(linkReader.getDestinationNode().getName());
					if (destId == null)
						throw new ServiceException("Could not find a valid destination node for the relationship");
					relationship.setDstNode(destId);
					tryNetworkOperation(() -> nodeRelationship.postXml(relationship, ClientResponse.class), Status.OK);
				}
			}
			// Finally set the current graph to a valid one if all was
			// successfully completed
			currentNffg = name;
		} catch (RuntimeException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean testReachability(String srcName, String destName)
			throws UnknownNameException, ServiceException, NoGraphException {
		if (getCurrentGraphName() == null)
			throw new NoGraphException("No graph is currently loaded");

		String srcID = nodeIds.get(srcName);
		String destID = nodeIds.get(destName);
		if ((srcID == null) || (destID == null))
			throw new UnknownNameException("Could not find at least one node in the loaded nffg graph");

		Paths paths = tryNetworkOperation(() -> resource.nodeNodeidPaths(srcID).getAsXml(destID, ClientResponse.class),
				Status.OK, Paths.class);
		if (paths.getPath().size() > 0)
			return true;
		return false;
	}

	@Override
	public String getCurrentGraphName() {
		return currentNffg;
	}

	/**
	 * Helper method used for network operations which tries for
	 * {@link ReachabilityTesterClient#NETWORK_TRIES} number of times the
	 * operation which could fail temporarily
	 * 
	 * @param operation
	 *            Network request to be executed.
	 * @param expected
	 *            Expected HTTP status code in case the operation completes with
	 *            success.
	 * @param returnType
	 *            Return type for the request.
	 * @return A response object of return type {@code returnType}.
	 * @throws ServiceException
	 *             if the operation failed.
	 */
	private <T> T tryNetworkOperation(Supplier<ClientResponse> operation, StatusType expected, Class<T> returnType)
			throws ServiceException {
		int tryCount = NETWORK_TRIES;
		ClientResponse response = null;
		while (tryCount-- > 0) {
			try {
				response = operation.get();
			} catch (WebApplicationException e) {
				continue;
			}
			if (response.getStatus() == expected.getStatusCode()) {
				if (!ClientResponse.class.isAssignableFrom(returnType)) {
					return response.getEntity(returnType);
				} else {
					return returnType.cast(response);
				}
			}
		}
		throw new ServiceException(
				"Webservice operation failed. Expected: " + expected + ", but was: " + response.getStatusInfo());
	}

	private ClientResponse tryNetworkOperation(Supplier<ClientResponse> operation, StatusType expected)
			throws ServiceException {
		return tryNetworkOperation(operation, expected, ClientResponse.class);
	}
}
