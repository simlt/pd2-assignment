package it.polito.dp2.NFFG.sol3.client1;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.GregorianCalendar;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

import it.polito.dp2.NFFG.LinkReader;
import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.NffgVerifier;
import it.polito.dp2.NFFG.NffgVerifierException;
import it.polito.dp2.NFFG.NffgVerifierFactory;
import it.polito.dp2.NFFG.NodeReader;
import it.polito.dp2.NFFG.PolicyReader;
import it.polito.dp2.NFFG.ReachabilityPolicyReader;
import it.polito.dp2.NFFG.TraversalPolicyReader;
import it.polito.dp2.NFFG.lab3.AlreadyLoadedException;
import it.polito.dp2.NFFG.lab3.ServiceException;
import it.polito.dp2.NFFG.lab3.UnknownNameException;
import it.polito.dp2.NFFG.sol3.client.ws.FunctionalType;
import it.polito.dp2.NFFG.sol3.client.ws.Link;
import it.polito.dp2.NFFG.sol3.client.ws.Localhost_NffgServiceRest;
import it.polito.dp2.NFFG.sol3.client.ws.Localhost_NffgServiceRest.Nffgs;
import it.polito.dp2.NFFG.sol3.client.ws.Localhost_NffgServiceRest.Policies;
import it.polito.dp2.NFFG.sol3.client.ws.NffgRequest;
import it.polito.dp2.NFFG.sol3.client.ws.Node;
import it.polito.dp2.NFFG.sol3.client.ws.Policy;
import it.polito.dp2.NFFG.sol3.client.ws.PolicyRef;
import it.polito.dp2.NFFG.sol3.client.ws.Verification;
import it.polito.dp2.NFFG.sol3.client.ws.VerificationRequest;

public class NFFGClient implements it.polito.dp2.NFFG.lab3.NFFGClient {
	private NffgVerifier verifier;
	private Nffgs nffgs;
	private Policies policies;
	private Localhost_NffgServiceRest.Verification verification;
	private Client client;

	public NFFGClient(String NffgServiceURL) throws URISyntaxException, NffgVerifierException {
		verifier = NffgVerifierFactory.newInstance().newNffgVerifier();
		URI uri = new URI(NffgServiceURL);
		client = Localhost_NffgServiceRest.createClient();
		nffgs = Localhost_NffgServiceRest.nffgs(client, uri);
		policies = Localhost_NffgServiceRest.policies(client, uri);
		verification = Localhost_NffgServiceRest.verification(client, uri);
	}

	@Override
	public void loadNFFG(String name) throws UnknownNameException, AlreadyLoadedException, ServiceException {
		NffgReader nffgReader = verifier.getNffg(name);
		// Check name first in order to avoid any server modification in case of
		// exception
		if (nffgReader == null)
			throw new UnknownNameException("Could not find the requested NFFG");
		loadNFFG(nffgReader);
	}

	@Override
	public void loadAll() throws AlreadyLoadedException, ServiceException {
		// Load NFFGs
		for (NffgReader nffgItr : verifier.getNffgs()) {
			loadNFFG(nffgItr);
		}

		// Load policies
		for (PolicyReader policyItr : verifier.getPolicies()) {
			Policy policy = createPolicy(policyItr);
			String nffgName = policyItr.getNffg().getName();
			loadPolicy(nffgName, policy);
		}
	}

	@Override
	public void loadReachabilityPolicy(String name, String nffgName, boolean isPositive, String srcNodeName,
			String dstNodeName) throws UnknownNameException, ServiceException {
		Policy policy = createReachabilityPolicy(name, nffgName, isPositive, srcNodeName, dstNodeName);
		loadPolicy(nffgName, policy);
	}

	@Override
	public void unloadReachabilityPolicy(String name) throws UnknownNameException, ServiceException {
		try {
			ClientResponse response = policies.name(name).deleteAsXml(ClientResponse.class);
		} catch (WebApplicationException e) {
			throw new ServiceException("Error while unloading policy on remote service");
		}
	}

	@Override
	public boolean testReachabilityPolicy(String name) throws UnknownNameException, ServiceException {
		try {
			VerificationRequest request = new VerificationRequest();
			PolicyRef policyRef = new PolicyRef();
			policyRef.setName(name);
			request.getPolicyRef().add(policyRef);
			ClientResponse response = verification.putXmlAs(request, ClientResponse.class);
			Policy policy = policies.name(name).getAsPolicy();
			return policy.getVerification().getResult().isValue();
		} catch (WebApplicationException e) {
			if (e.getResponse().getStatusInfo() == Status.BAD_REQUEST) {
				throw new UnknownNameException("Policy " + name + " was NOT found on remote service");
			}
			throw new ServiceException("Error while unloading policy on remote service");
		}
	}

	private NffgRequest createNffgRequest(NffgReader nffgReader) {
		NffgRequest nffg = new NffgRequest();
		nffg.setName(nffgReader.getName());

		// Add nodes
		for (NodeReader nodeItr : nffgReader.getNodes()) {
			Node node = new Node();
			node.setName(nodeItr.getName());
			node.setType(FunctionalType.fromValue(nodeItr.getFuncType().value()));
			nffg.getNode().add(node);

			// Add links
			for (LinkReader linkItr : nodeItr.getLinks()) {
				Link link = new Link();
				link.setName(linkItr.getName());
				link.setDestination(linkItr.getDestinationNode().getName());
				node.getLink().add(link);
			}
		}
		return nffg;
	}

	// Read policies
	private Policy createPolicy(PolicyReader policyReader) {
		// Every policy should at least be of reachability type
		ReachabilityPolicyReader reachabilityReader = (ReachabilityPolicyReader) policyReader;
		Policy policy = createReachabilityPolicy(reachabilityReader.getName(), reachabilityReader.getNffg().getName(),
				reachabilityReader.isPositive(), reachabilityReader.getSourceNode().getName(),
				reachabilityReader.getDestinationNode().getName());

		// Optional traversal type
		if (policyReader instanceof TraversalPolicyReader) {
			// Add all traversal types
			for (it.polito.dp2.NFFG.FunctionalType traversalItr : ((TraversalPolicyReader) policyReader)
					.getTraversedFuctionalTypes()) {
				Policy.Traversal traversal = new Policy.Traversal();
				traversal.setType(FunctionalType.valueOf(traversalItr.value()));
				policy.getTraversal().add(traversal);
			}
		}

		// Optional verification
		if (policyReader.getResult() != null) {
			Verification verification = new Verification();
			Verification.Result result = new Verification.Result();
			result.setValue(policyReader.getResult().getVerificationResult());
			verification.setResult(result);
			verification.setMsg(policyReader.getResult().getVerificationResultMsg());
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(policyReader.getResult().getVerificationTime().getTime());
			try {
				verification.setLastVerification(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
			policy.setVerification(verification);
		}
		return policy;
	}

	private Policy createReachabilityPolicy(String name, String nffgName, boolean isPositive, String srcNodeName,
			String dstNodeName) {
		Policy policy = new Policy();
		policy.setName(name);
		policy.setNffg(nffgName);
		policy.setIsPositive(isPositive);

		// Implicit reachability type
		Policy.Reachability reachability = new Policy.Reachability();
		reachability.setSource(srcNodeName);
		reachability.setDestination(dstNodeName);
		policy.setReachability(reachability);

		return policy;
	}

	private void loadNFFG(NffgReader nffgReader) throws AlreadyLoadedException, ServiceException {
		try {
			NffgRequest nffgRequest = createNffgRequest(nffgReader);
			ClientResponse response = nffgs.postXml(nffgRequest, ClientResponse.class);
		} catch (WebApplicationException e) {
			if (e.getResponse().getStatusInfo().equals(Status.CONFLICT))
				throw new AlreadyLoadedException(
						"NFFG " + nffgReader.getName() + "is already loaded on remote service");
			throw new ServiceException("Error while loading nffg on remote service");
		}
	}

	private void loadPolicy(String nffgName, Policy policy) throws ServiceException {
		int tries = 1;
		while (tries-- >= 0) {
			try {
				ClientResponse response = policies.postXml(policy, ClientResponse.class);
				return;
			} catch (WebApplicationException e) {
				if (e.getResponse().getStatusInfo().equals(Status.CONFLICT)) {
					// throw new AlreadyLoadedException("Policy " + nffgName +
					// "is already loaded on remote service");
					// Overwrite the policy in case of already existing policy
					policies.name(policy.getName()).deleteAsPolicy();
					// Now try resending request
				} else
					break;
			}
		}
		throw new ServiceException("Error while loading policy on remote service");
	}
}
