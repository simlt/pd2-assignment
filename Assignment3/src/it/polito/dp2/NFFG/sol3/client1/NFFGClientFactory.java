package it.polito.dp2.NFFG.sol3.client1;

import java.net.URISyntaxException;

import it.polito.dp2.NFFG.NffgVerifierException;
import it.polito.dp2.NFFG.lab3.NFFGClient;
import it.polito.dp2.NFFG.lab3.NFFGClientException;

public class NFFGClientFactory extends it.polito.dp2.NFFG.lab3.NFFGClientFactory {
	public NFFGClientFactory() {
	}

	@Override
	public NFFGClient newNFFGClient() throws NFFGClientException {
		String serviceURL = System.getProperty("it.polito.dp2.NFFG.lab3.URL");
		if (serviceURL == null) {
			serviceURL = "http://localhost:8080/NffgService/rest/";
		}
		try {
			return new it.polito.dp2.NFFG.sol3.client1.NFFGClient(serviceURL);
		} catch (URISyntaxException | NffgVerifierException e) {
			System.out.println("Could not initialize NFFGClient:\n" + e.getMessage());
			throw new NFFGClientException(e);
		}
	}

}
