package it.polito.dp2.NFFG.sol3.client2;

import java.util.Calendar;
import java.util.Set;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;

import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.NffgVerifier;
import it.polito.dp2.NFFG.NffgVerifierException;
import it.polito.dp2.NFFG.PolicyReader;
import it.polito.dp2.NFFG.sol3.client2.parser.NffgVerifierParser;
import it.polito.dp2.NFFG.sol3.jaxb.NffgInfo;

public class NffgVerifierClient implements NffgVerifier {
	private NffgInfo nffgInfo;
	private NffgVerifierParser nffgVerifier;

	public NffgVerifierClient(String NffgServiceURL) throws NffgVerifierException {
		UriBuilder uri = UriBuilder.fromUri(NffgServiceURL);
		uri.path("/nffgInfo");
		/*
		 * Client client = Localhost_NffgServiceRest.createClient(); nffgInfo =
		 * Localhost_NffgServiceRest.nffgInfo(client, uri).getAsNffgInfo();
		 * 
		 * JAXBElement<it.polito.dp2.NFFG.sol1.jaxb.NffgInfo> jaxbNffgInfo = new
		 * JAXBElement( new QName("root"),
		 * it.polito.dp2.NFFG.sol1.jaxb.NffgInfo.class, nffgInfo);
		 */

		Client client = new Client();
		try {
			nffgInfo = client.resource(uri.build()).accept(MediaType.APPLICATION_XML).get(NffgInfo.class);
			nffgVerifier = new NffgVerifierParser(nffgInfo);
		} catch (WebApplicationException e) {
			throw new NffgVerifierException(e);
		}
	}

	@Override
	public NffgReader getNffg(String name) {
		return nffgVerifier.getNffg(name);
	}

	@Override
	public Set<NffgReader> getNffgs() {
		return nffgVerifier.getNffgs();
	}

	@Override
	public Set<PolicyReader> getPolicies() {
		return nffgVerifier.getPolicies();
	}

	@Override
	public Set<PolicyReader> getPolicies(String name) {
		return nffgVerifier.getPolicies(name);
	}

	@Override
	public Set<PolicyReader> getPolicies(Calendar date) {
		return nffgVerifier.getPolicies(date);
	}

}
