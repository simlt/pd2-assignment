package it.polito.dp2.NFFG.sol3.client2;

import it.polito.dp2.NFFG.NffgVerifier;
import it.polito.dp2.NFFG.NffgVerifierException;

public class NffgVerifierFactory extends it.polito.dp2.NFFG.NffgVerifierFactory {

	@Override
	public NffgVerifier newNffgVerifier() throws NffgVerifierException {
		String serviceURL = System.getProperty("it.polito.dp2.NFFG.lab3.URL");
		if (serviceURL == null) {
			serviceURL = "http://localhost:8080/NffgService/rest/";
		}
		return new NffgVerifierClient(serviceURL);
	}
}
