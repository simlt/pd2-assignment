package it.polito.dp2.NFFG.sol3.client2.parser;

import it.polito.dp2.NFFG.LinkReader;
import it.polito.dp2.NFFG.NodeReader;
import it.polito.dp2.NFFG.sol3.jaxb.Link;

public class LinkParser implements LinkReader {
	Link link;
	NodeReader source;
	NodeReader destination;

	public LinkParser(Link link, NodeReader source, NodeReader destination) {
		this.link = link;
		this.source = source;
		this.destination = destination;
	}

	@Override
	public String getName() {
		return link.getName();
	}

	@Override
	public NodeReader getDestinationNode() {
		return destination;
	}

	@Override
	public NodeReader getSourceNode() {
		return source;
	}

}
