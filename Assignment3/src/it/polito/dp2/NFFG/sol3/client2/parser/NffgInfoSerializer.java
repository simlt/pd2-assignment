/**
 * 
 */
package it.polito.dp2.NFFG.sol3.client2.parser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import it.polito.dp2.NFFG.LinkReader;
import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.NffgVerifier;
import it.polito.dp2.NFFG.NffgVerifierException;
import it.polito.dp2.NFFG.NffgVerifierFactory;
import it.polito.dp2.NFFG.NodeReader;
import it.polito.dp2.NFFG.PolicyReader;
import it.polito.dp2.NFFG.ReachabilityPolicyReader;
import it.polito.dp2.NFFG.TraversalPolicyReader;
import it.polito.dp2.NFFG.sol3.jaxb.FunctionalType;
import it.polito.dp2.NFFG.sol3.jaxb.Link;
import it.polito.dp2.NFFG.sol3.jaxb.Nffg;
import it.polito.dp2.NFFG.sol3.jaxb.NffgInfo;
import it.polito.dp2.NFFG.sol3.jaxb.Node;
import it.polito.dp2.NFFG.sol3.jaxb.Policy;
import it.polito.dp2.NFFG.sol3.jaxb.Verification;

/**
 * @author Simon Mezzomo
 *
 */
public class NffgInfoSerializer {
	private NffgVerifier monitor;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		NffgInfoSerializer nffgSerializer;
		// FileOutputStream file;
		String filename = "";

		if (args.length == 1)
			filename = args[0];

		// NFFG instance
		try {
			nffgSerializer = new NffgInfoSerializer();
		} catch (NffgVerifierException e) {
			System.err.println("Could not instantiate data generator.");
			e.printStackTrace();
			return;
		}

		// JAXB binding
		try {
			JAXBContext jc = JAXBContext.newInstance("it.polito.dp2.NFFG.sol1.jaxb");
			NffgInfo nffgInfo = new NffgInfo();

			Map<String, Nffg> nffgMap = new HashMap<>();

			// Read NFFGs
			for (NffgReader nffgItr : nffgSerializer.monitor.getNffgs()) {
				Nffg nffg = new Nffg();
				nffg.setName(nffgItr.getName());
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(nffgItr.getUpdateTime().getTime());
				try {
					nffg.setLastUpdate(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
				} catch (DatatypeConfigurationException e) {
					e.printStackTrace();
					continue; // Skip nffg
				}
				nffgInfo.getNffg().add(nffg);
				nffgMap.put(nffg.getName(), nffg);

				// Add nodes
				for (NodeReader nodeItr : nffgItr.getNodes()) {
					Node node = new Node();
					node.setName(nodeItr.getName());
					node.setType(FunctionalType.fromValue(nodeItr.getFuncType().value()));
					nffg.getNode().add(node);

					// Add links
					for (LinkReader linkItr : nodeItr.getLinks()) {
						Link link = new Link();
						link.setName(linkItr.getName());
						link.setDestination(linkItr.getDestinationNode().getName());
						node.getLink().add(link);
					}
				}
			}

			// Read policies
			for (PolicyReader policyItr : nffgSerializer.monitor.getPolicies()) {
				// Every policy should at least be of reachability type
				ReachabilityPolicyReader reachabilityItr = (ReachabilityPolicyReader) policyItr;
				Policy policy = new Policy();
				policy.setName(reachabilityItr.getName());
				policy.setIsPositive(reachabilityItr.isPositive());

				// Implicit reachability type
				Policy.Reachability reachability = new Policy.Reachability();
				reachability.setSource(reachabilityItr.getSourceNode().getName());
				reachability.setDestination(reachabilityItr.getDestinationNode().getName());
				policy.setReachability(reachability);

				// Optional traversal type
				if (policyItr instanceof TraversalPolicyReader) {
					// Add all traversal types
					for (it.polito.dp2.NFFG.FunctionalType traversalItr : ((TraversalPolicyReader) policyItr)
							.getTraversedFuctionalTypes()) {
						Policy.Traversal traversal = new Policy.Traversal();
						traversal.setType(FunctionalType.valueOf(traversalItr.value()));
						policy.getTraversal().add(traversal);
					}
				}

				// Optional verification
				if (policyItr.getResult() != null) {
					Verification verification = new Verification();
					Verification.Result result = new Verification.Result();
					result.setValue(policyItr.getResult().getVerificationResult());
					verification.setResult(result);
					verification.setMsg(policyItr.getResult().getVerificationResultMsg());
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime(policyItr.getResult().getVerificationTime().getTime());
					try {
						verification.setLastVerification(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
					} catch (DatatypeConfigurationException e) {
						e.printStackTrace();
					}
					policy.setVerification(verification);
				}

				// Add policy to its nffg
				nffgMap.get(policyItr.getNffg().getName()).getPolicy().add(policy);
			}

			// Marshalling
			Marshaller m = jc.createMarshaller();
			// human-readable format
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			// Schema location
			m.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "xsd/nffgInfo.xsd");

			// Save to output file
			try (BufferedWriter br = new BufferedWriter(new FileWriter(filename))) {
				System.out.println("Outputting on file: " + filename);
				m.marshal(nffgInfo, br);
			} catch (IOException e) {
				System.err.println("Could not create output file.");
			} catch (JAXBException e) {
				System.err.println("Could not marshal JAXB object.");
				e.printStackTrace();
			}
		} catch (JAXBException e) {
			System.err.println("Could not bind to JAXB context.");
			return;
		}
	}

	/**
	 * Default constructror
	 * 
	 * @throws NffgVerifierException
	 */
	public NffgInfoSerializer() throws NffgVerifierException {
		NffgVerifierFactory factory = NffgVerifierFactory.newInstance();
		monitor = factory.newNffgVerifier();
	}
}
