package it.polito.dp2.NFFG.sol3.client2.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import it.polito.dp2.NFFG.NffgVerifier;
import it.polito.dp2.NFFG.NffgVerifierException;
import it.polito.dp2.NFFG.sol3.jaxb.NffgInfo;

public class NffgVerifierFactory extends it.polito.dp2.NFFG.NffgVerifierFactory {

	@Override
	public NffgVerifier newNffgVerifier() throws NffgVerifierException {
		String schemafile = "xsd/nffgInfo.xsd";
		try {
			String dir = System.getProperty("it.polito.dp2.NFFG.sol1.NffgInfo.file");
			BufferedReader reader = new BufferedReader(new FileReader(dir));

			JAXBContext jc = JAXBContext.newInstance("it.polito.dp2.NFFG.sol1.jaxb");
			Unmarshaller u = jc.createUnmarshaller();
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(new File(schemafile));
			u.setSchema(schema);

			NffgInfo nffgInfo = (NffgInfo) u.unmarshal(reader);

			return new NffgVerifierParser(nffgInfo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NffgVerifierException(e);
		}
	}
}
