package it.polito.dp2.NFFG.sol3.client2.parser;

import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.NffgVerifier;
import it.polito.dp2.NFFG.PolicyReader;
import it.polito.dp2.NFFG.sol3.jaxb.Nffg;
import it.polito.dp2.NFFG.sol3.jaxb.NffgInfo;

public class NffgVerifierParser implements NffgVerifier {
	private Map<String, NffgReader> nffgMap = new HashMap<>();
	// map with Set of policies grouped by NFFG
	private Map<String, Set<PolicyReader>> policyMap = new HashMap<>();
	private Set<PolicyReader> policies = new HashSet<>();

	public NffgVerifierParser(NffgInfo nffgInfo) {
		// Parse Nffgs
		nffgMap = nffgInfo.getNffg().parallelStream().collect(Collectors.toMap(Nffg::getName, NffgParser::new));
		// Parse Policies
		for (Nffg n : nffgInfo.getNffg()) {
			policyMap.put(n.getName(), n.getPolicy().parallelStream().map(p -> {
				PolicyParser policy;
				if (p.getTraversal().isEmpty())
					policy = new ReachabilityPolicyParser(p, getNffg(n.getName()));
				else
					policy = new TraversalPolicyParser(p, getNffg(n.getName()));
				return policy;
			}).collect(Collectors.toSet()));
		}
		policies = policyMap.values().parallelStream().flatMap(p -> p.stream()).collect(Collectors.toSet());
	}

	@Override
	public NffgReader getNffg(String arg0) {
		return nffgMap.get(arg0);
	}

	@Override
	public Set<NffgReader> getNffgs() {
		return new HashSet<NffgReader>(nffgMap.values());
	}

	@Override
	public Set<PolicyReader> getPolicies() {
		return policies;
	}

	@Override
	public Set<PolicyReader> getPolicies(String arg0) {
		return policyMap.get(arg0);
	}

	@Override
	public Set<PolicyReader> getPolicies(Calendar arg0) {
		// Policy must be verified after the arg0 time
		Set<PolicyReader> set = policies.parallelStream().filter(p -> p.getNffg().getUpdateTime().compareTo(arg0) > 0)
				.collect(Collectors.toSet());
		return set.size() == 0 ? null : set;
	}

}
