package it.polito.dp2.NFFG.sol3.client2.parser;

import java.util.Set;

import it.polito.dp2.NFFG.FunctionalType;
import it.polito.dp2.NFFG.LinkReader;
import it.polito.dp2.NFFG.NodeReader;
import it.polito.dp2.NFFG.sol3.jaxb.Node;

public class NodeParser implements NodeReader {
	Node node;
	Set<LinkReader> links;

	public NodeParser(Node node) {
		this.node = node;
	}

	protected void setLink(Set<LinkReader> links) {
		this.links = links;
	}

	@Override
	public String getName() {
		return node.getName();
	}

	@Override
	public FunctionalType getFuncType() {
		return FunctionalType.valueOf(node.getType().value());
	}

	@Override
	public Set<LinkReader> getLinks() {
		return links;
	}

}
