package it.polito.dp2.NFFG.sol3.client2.parser;

import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.PolicyReader;
import it.polito.dp2.NFFG.VerificationResultReader;
import it.polito.dp2.NFFG.sol3.jaxb.Policy;

public class PolicyParser implements PolicyReader {
	private Policy policy;
	private NffgReader nffg;
	private VerificationResultReader result = null;

	public PolicyParser(Policy policy, NffgReader nffg) {
		this.policy = policy;
		this.nffg = nffg;
		if (policy.getVerification() != null)
			result = new VerificationResultParser(policy.getVerification(), this);
	}

	@Override
	public String getName() {
		return policy.getName();
	}

	@Override
	public NffgReader getNffg() {
		return nffg;
	}

	@Override
	public VerificationResultReader getResult() {
		return result;
	}

	@Override
	public Boolean isPositive() {
		return policy.isIsPositive();
	}

}
