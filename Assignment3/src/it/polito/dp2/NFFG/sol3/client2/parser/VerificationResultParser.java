package it.polito.dp2.NFFG.sol3.client2.parser;

import java.util.Calendar;

import it.polito.dp2.NFFG.PolicyReader;
import it.polito.dp2.NFFG.VerificationResultReader;
import it.polito.dp2.NFFG.sol3.jaxb.Verification;

public class VerificationResultParser implements VerificationResultReader {
	Verification result;
	PolicyReader policy;

	public VerificationResultParser(Verification result, PolicyReader policy) {
		this.result = result;
		this.policy = policy;
	}

	@Override
	public PolicyReader getPolicy() {
		return policy;
	}

	@Override
	public Boolean getVerificationResult() {
		return result.getResult().isValue();
	}

	@Override
	public String getVerificationResultMsg() {
		return result.getMsg();
	}

	@Override
	public Calendar getVerificationTime() {
		return result.getLastVerification().toGregorianCalendar();
	}

}
