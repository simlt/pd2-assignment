package it.polito.dp2.NFFG.sol3.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import it.polito.dp2.NFFG.sol3.jaxb.NffgInfo;
import it.polito.dp2.NFFG.sol3.service.model.NffgService;

@Path("/nffgInfo")
public class NffgInfoResource {
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public NffgInfo getNffgInfo() {
		return NffgService.getNffgInfo();
	}
}
