package it.polito.dp2.NFFG.sol3.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import it.polito.dp2.NFFG.sol3.jaxb.Nffg;
import it.polito.dp2.NFFG.sol3.jaxb.NffgRequest;
import it.polito.dp2.NFFG.sol3.service.model.NffgEx;
import it.polito.dp2.NFFG.sol3.service.model.NffgService;

@Path("/nffgs")
public class NffgResource {
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response postNffg(NffgRequest nffgReq, @Context UriInfo uriInfo) {
		Nffg nffg = NffgService.addNffg(new NffgEx(nffgReq));
		UriBuilder uri = uriInfo.getAbsolutePathBuilder().path(nffg.getName());
		return Response.created(uri.build()).entity(nffg).build();
	}

	@GET
	@Path("{name: [a-zA-Z][a-zA-Z0-9]*}")
	@Produces(MediaType.APPLICATION_XML)
	public Nffg getNffgByName(@PathParam("name") String name) {
		return NffgService.getNffg(name);
	}
}
