package it.polito.dp2.NFFG.sol3.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import it.polito.dp2.NFFG.sol3.jaxb.VerificationRequest;
import it.polito.dp2.NFFG.sol3.service.model.NffgService;

@Path("/verification")
public class NffgVerificationResource {
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public Response verifyPolicies(VerificationRequest verificationReq, @Context UriInfo uriInfo) {
		NffgService.verifyPolicies(verificationReq.getPolicyRef());
		return Response.ok().build();
	}
}
