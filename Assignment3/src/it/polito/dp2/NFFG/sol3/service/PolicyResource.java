package it.polito.dp2.NFFG.sol3.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import it.polito.dp2.NFFG.sol3.jaxb.Policy;
import it.polito.dp2.NFFG.sol3.service.model.NffgEx.DuplicatePolicyException;
import it.polito.dp2.NFFG.sol3.service.model.NffgEx.InvalidPolicyException;
import it.polito.dp2.NFFG.sol3.service.model.NffgInfoEx.InvalidNffgException;
import it.polito.dp2.NFFG.sol3.service.model.NffgService;
import it.polito.dp2.NFFG.sol3.service.model.PolicyEx;

@Path("/policies")
public class PolicyResource {
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response postPolicy(Policy policyreq, @Context UriInfo uriInfo) {
		try {
			if (policyreq.getNffg() == null) {
				throw new WebApplicationException(Response.status(Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN_TYPE)
						.entity("The policy \"" + policyreq.getName() + "\" must provide a \"nffg\" name attribute")
						.build());
			}
			Policy policy = NffgService.addPolicy(new PolicyEx(policyreq, policyreq.getNffg()));
			UriBuilder uri = uriInfo.getAbsolutePathBuilder().path(policy.getName());
			return Response.created(uri.build()).entity(policy).build();
		} catch (DuplicatePolicyException e) {
			throw new WebApplicationException(Response.status(Status.CONFLICT).type(MediaType.TEXT_PLAIN_TYPE)
					.entity("Duplicate name \"" + policyreq.getName() + "\" for policy").build());
		} catch (InvalidNffgException e) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN_TYPE)
					.entity("The policy \"" + policyreq.getName() + "\" referenced an invalid NFFG").build());
		} catch (InvalidPolicyException e) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN_TYPE)
					.entity("The policy \"" + policyreq.getName() + "\" referenced an invalid node in the NFFG")
					.build());
		}
	}

	@GET
	@Path("{name: [a-zA-Z][a-zA-Z0-9]*}")
	@Produces(MediaType.APPLICATION_XML)
	public Policy getPolicyByName(@PathParam("name") String name) {
		return NffgService.getPolicy(name);
	}

	@DELETE
	@Path("{name: [a-zA-Z][a-zA-Z0-9]*}")
	@Produces(MediaType.APPLICATION_XML)
	public Policy deletePolicyByName(@PathParam("name") String name) {
		return NffgService.deletePolicy(name);
	}

	@PUT
	@Path("{name: [a-zA-Z][a-zA-Z0-9]*}")
	@Consumes(MediaType.APPLICATION_XML)
	public Policy setPolicy(@PathParam("name") String policyName, Policy policy) {
		try {
			String nffgName = NffgService.getNffgNameRef(policy.getName());
			if (policy.getNffg() != null) {
				nffgName = policy.getNffg();
			}
			Policy newPolicy = NffgService.setPolicy(policyName, new PolicyEx(policy, nffgName));
			return newPolicy;
		} catch (InvalidPolicyException e) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN_TYPE)
					.entity("The policy \"" + policyName + "\" referenced an invalid node in the NFFG").build());
		}
	}
}
