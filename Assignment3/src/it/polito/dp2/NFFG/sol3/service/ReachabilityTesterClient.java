package it.polito.dp2.NFFG.sol3.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.UriBuilderException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;

import it.polito.dp2.NFFG.LinkReader;
import it.polito.dp2.NFFG.NffgReader;
import it.polito.dp2.NFFG.NodeReader;
import it.polito.dp2.NFFG.lab2.NoGraphException;
import it.polito.dp2.NFFG.lab2.ServiceException;
import it.polito.dp2.NFFG.lab2.UnknownNameException;
import it.polito.dp2.NFFG.sol3.service.neo4j.Labels;
import it.polito.dp2.NFFG.sol3.service.neo4j.Localhost_Neo4JXMLRest;
import it.polito.dp2.NFFG.sol3.service.neo4j.Localhost_Neo4JXMLRest.Resource;
import it.polito.dp2.NFFG.sol3.service.neo4j.Localhost_Neo4JXMLRest.Resource.NodeNodeidRelationship;
import it.polito.dp2.NFFG.sol3.service.neo4j.Node;
import it.polito.dp2.NFFG.sol3.service.neo4j.Paths;
import it.polito.dp2.NFFG.sol3.service.neo4j.Property;
import it.polito.dp2.NFFG.sol3.service.neo4j.Relationship;

public class ReachabilityTesterClient {
	private Resource resource;
	/**
	 * Used to cache locally node ids needed to create node relationships. 1st
	 * key: NffgName which gives access to the map of: Key: NodeName, value:
	 * NodeID on NEO4J
	 */
	private Map<String, String> nffgIds = new HashMap<>();
	private Map<String, Map<String, String>> nffgNodesCache = new HashMap<>();

	public ReachabilityTesterClient(String Neo4JURL) throws URISyntaxException {
		if (Neo4JURL != null) {
			Client client = Localhost_Neo4JXMLRest.createClient();
			this.resource = Localhost_Neo4JXMLRest.resource(client, new URI(Neo4JURL));
		} else {
			this.resource = Localhost_Neo4JXMLRest.resource();
		}
		this.resource.nodes().deleteAsXml(ClientResponse.class);
	}

	public void loadNFFG(NffgReader nffg) throws ServiceException {
		try {
			Map<String, String> nodeIds = new HashMap<>();
			String name = nffg.getName();

			// Upload new nffg to DB
			if (isGraphLoaded(name))
				throw new ServiceException("Trying to load a NFFG with an already existing name");

			// Create the NFFG name node
			Node node = new Node();
			Property property = new Property();
			property.setName("name");
			property.setValue(name);
			node.getProperty().add(property);
			Node nffgNodeResponse = resource.node().postXmlAsNode(node);
			String nffgNodeId = nffgNodeResponse.getId();
			Labels labels = new Labels();
			labels.getValue().add("NFFG");
			ClientResponse clientResponse = resource.nodeNodeidLabel(nffgNodeId).postXml(labels, ClientResponse.class);
			if (clientResponse.getStatusInfo() != Status.NO_CONTENT)
				throw new ServiceException(
						"Could not create label successfully. Expected NO_CONTENT, but response was: "
								+ clientResponse.getStatusInfo().getReasonPhrase());

			Relationship nffgRelationship = new Relationship();
			nffgRelationship.setType("belongs");
			nffgRelationship.setDstNode(nffgNodeId);

			// Create NFFG nodes
			for (NodeReader nodeReader : nffg.getNodes()) {
				property.setName("name");
				property.setValue(nodeReader.getName());
				node.getProperty().add(property);
				String nodeId = resource.node().postXmlAsNode(node).getId();
				// Also link each node to main NFFG name node to support
				// multiple NFFGs
				nffgRelationship.setSrcNode(nodeId);
				resource.nodeNodeidRelationship(nodeId).postXmlAsRelationship(nffgRelationship);
				// Cache node id from response for later use
				nodeIds.put(nodeReader.getName(), nodeId);
			}

			// Create node links after all nodes are loaded
			Relationship linkRelationship = new Relationship();
			linkRelationship.setType("Link");
			for (NodeReader nodeReader : nffg.getNodes()) {
				NodeNodeidRelationship nodeRelationship = resource
						.nodeNodeidRelationship(nodeIds.get(nodeReader.getName()));
				for (LinkReader linkReader : nodeReader.getLinks()) {
					String destId = nodeIds.get(linkReader.getDestinationNode().getName());
					if (destId == null)
						throw new ServiceException("Could not find a valid destination node for the link relationship");
					linkRelationship.setDstNode(destId);
					nodeRelationship.postXmlAsRelationship(linkRelationship);
				}
			}
			// Finally add the current graph if all was successfully completed
			nffgIds.put(name, nffgNodeId);
			nffgNodesCache.put(name, nodeIds);
		} catch (UriBuilderException | WebApplicationException e) {
			throw new ServiceException(e.getMessage());
		}
	}

	public boolean testReachability(String nffgName, String srcName, String destName)
			throws UnknownNameException, ServiceException, NoGraphException {
		if (!isGraphLoaded(nffgName))
			throw new NoGraphException("The requested NFFG graph does not exist");

		Map<String, String> nodeIds = nffgNodesCache.get(nffgName);
		String srcID = nodeIds.get(srcName);
		String destID = nodeIds.get(destName);
		if ((srcID == null) || (destID == null))
			throw new UnknownNameException("Could not find at least one node in the loaded nffg graph");

		try {
			Paths paths = resource.nodeNodeidPaths(srcID).getAsPaths(destID);
			if (paths.getPath().size() > 0)
				return true;
		} catch (WebApplicationException e) {
			throw new ServiceException("NEO4J service not reachable");
		}
		return false;
	}

	private boolean isGraphLoaded(String nffgName) {
		return nffgNodesCache.containsKey(nffgName);
	}
}
