package it.polito.dp2.NFFG.sol3.service.model;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import it.polito.dp2.NFFG.sol3.jaxb.Nffg;
import it.polito.dp2.NFFG.sol3.jaxb.NffgRequest;

public class NffgEx {
	// Store policies
	private Map<String, PolicyEx> policyMap = new ConcurrentHashMap<>();
	private Nffg nffg = new Nffg();

	public NffgEx(NffgRequest nffgReq) {
		nffg.setName(nffgReq.getName());
		try {
			GregorianCalendar gregorianCalendar = new GregorianCalendar();
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar).normalize();
			nffg.setLastUpdate(now);
		} catch (DatatypeConfigurationException e) {
			System.out.println(e.getMessage());
		}
		// for (Node node : nffgReq.getNode()) nffg.getNode().add(node);
		nffg.getNode().addAll(nffgReq.getNode());
	}

	protected Nffg getNffg() {
		synchronized (nffg) {
			return nffg;
		}
	}

	public String getName() {
		return nffg.getName();
	}

	public class DuplicatePolicyException extends Exception {
		private static final long serialVersionUID = 2650077946200115636L;
	}

	public class InvalidPolicyException extends Exception {
		private static final long serialVersionUID = 4839146648840783399L;
	}

	private void checkPolicy(PolicyEx policy) throws InvalidPolicyException {
		// Check Policy
		String src = policy.getPolicy().getReachability().getSource();
		String dst = policy.getPolicy().getReachability().getDestination();
		// NFFG is assumed to be immutable, no synchronization needed
		boolean validSrc = nffg.getNode().parallelStream().anyMatch(n -> src.equals(n.getName()));
		boolean validDst = nffg.getNode().parallelStream().anyMatch(n -> dst.equals(n.getName()));

		if (!validSrc || !validDst) {
			throw new InvalidPolicyException();
		}
	}

	public synchronized PolicyEx addPolicy(PolicyEx policy) throws DuplicatePolicyException, InvalidPolicyException {
		checkPolicy(policy);
		String name = policy.getPolicy().getName();
		/*
		 * Validate the policy before performing checkAndAddPolicyName, which
		 * will lock the policyName. The policy MUST complete add operation
		 * after this otherwise the resource becomes occupied with no valid
		 * policy
		 */
		if (!NffgService.checkAndAddPolicyName(policy)) {
			throw new DuplicatePolicyException();
		}
		policyMap.put(name, policy);
		nffg.getPolicy().add(policy.getPolicy());
		return policy;
	}

	public List<PolicyEx> getPolicies() {
		return new ArrayList<>(policyMap.values());
	}

	public PolicyEx getPolicy(String policyName) {
		PolicyEx policy = policyMap.get(policyName);
		if (policy == null) {
			throw new NotFoundException();
		}
		return policy;
	}

	public synchronized PolicyEx setPolicy(PolicyEx policy) throws InvalidPolicyException {
		checkPolicy(policy);
		PolicyEx old = policyMap.get(policy.getPolicy().getName());
		if (old == null)
			throw new NotFoundException();
		int i = nffg.getPolicy().indexOf(old.getPolicy());
		if (i < 0)
			throw new InternalServerErrorException("Error while updating policy.");

		checkPolicy(policy);
		nffg.getPolicy().set(i, policy.getPolicy());
		policyMap.put(policy.getPolicy().getName(), policy);
		return policy;
	}

	public synchronized PolicyEx deletePolicy(String policyName) {
		if (!policyMap.containsKey(policyName))
			throw new NotFoundException();
		PolicyEx policy = policyMap.remove(policyName);
		nffg.getPolicy().remove(policy.getPolicy());
		NffgService.removePolicyName(policyName);
		return policy;
	}
}
