package it.polito.dp2.NFFG.sol3.service.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.polito.dp2.NFFG.lab2.ServiceException;
import it.polito.dp2.NFFG.sol3.jaxb.NffgInfo;
import it.polito.dp2.NFFG.sol3.service.parser.NffgParser;

public class NffgInfoEx {
	private Map<String, NffgEx> nffgMap = new ConcurrentHashMap<>();
	// Set used to check that duplicate name policies are not inserted
	private Map<String, String> policyToNffgMap = new ConcurrentHashMap<>();
	private NffgInfo nffgInfo = new NffgInfo();

	protected NffgInfo getNffgInfo() {
		return nffgInfo;
	}

	// Synchronize in case of multiple adds
	public synchronized NffgEx addNffg(NffgEx nffg) {
		String nffgName = nffg.getName();
		if (nffgMap.containsKey(nffgName))
			throw new WebApplicationException(Response.status(Status.CONFLICT).type(MediaType.TEXT_PLAIN_TYPE)
					.entity("Duplicate name \"" + nffgName + "\" for NFFG").build());
		try {
			NffgParser nffgParser = new NffgParser(nffg.getNffg());
			NffgService.getReachabilityTesterClient().loadNFFG(nffgParser);
		} catch (ServiceException e) {
			throw new WebApplicationException(e,
					Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.TEXT_PLAIN_TYPE)
							.entity("Could not successfully load the NFFG on the NEO4J service.\n" + e.getMessage())
							.build());
		} catch (Exception e) {
			throw new WebApplicationException(Response.status(Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN_TYPE)
					.entity("Malformed NFFG content\n").build());
		}
		nffgMap.put(nffgName, nffg);
		nffgInfo.getNffg().add(nffg.getNffg());
		return nffg;
	}

	/**
	 * Indicates that the referenced NFFG doesn't exist.
	 */
	public class InvalidNffgException extends Exception {
		private static final long serialVersionUID = 4710757947342381087L;
	}

	public NffgEx getNffg(String name) throws InvalidNffgException {
		// get is thread safe
		NffgEx nffg = nffgMap.get(name);
		if (nffg == null)
			throw new InvalidNffgException();
		return nffg;
	}

	// This returns true if successful, otherwise false if there is a duplicate
	// policy name
	public boolean checkAndAddPolicyName(PolicyEx policy) {
		String nffgNameRef = policyToNffgMap.putIfAbsent(policy.getPolicy().getName(), policy.getNffgName());
		return nffgNameRef == null;
	}

	/*
	 * This isn't needed. We can assume that a NFFG graph will always be
	 * existing once stored public synchronized NffgEx deleteNffg(String name) {
	 * NffgEx nffg = nffgMap.remove(name); if (nffg == null) throw new
	 * NotFoundException(); nffgInfo.getNffg().remove(nffg.getNffg()); return
	 * nffg; }
	 */

	public String removePolicyName(String policyName) {
		return policyToNffgMap.remove(policyName);
	}

	// This returns null if the policy doesn't exist
	public String getNffgNameRef(String policyName) {
		return policyToNffgMap.get(policyName);
	}
}
