package it.polito.dp2.NFFG.sol3.service.model;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.polito.dp2.NFFG.lab2.ServiceException;
import it.polito.dp2.NFFG.sol3.jaxb.Nffg;
import it.polito.dp2.NFFG.sol3.jaxb.NffgInfo;
import it.polito.dp2.NFFG.sol3.jaxb.Policy;
import it.polito.dp2.NFFG.sol3.jaxb.PolicyRef;
import it.polito.dp2.NFFG.sol3.service.ReachabilityTesterClient;
import it.polito.dp2.NFFG.sol3.service.model.NffgEx.DuplicatePolicyException;
import it.polito.dp2.NFFG.sol3.service.model.NffgEx.InvalidPolicyException;
import it.polito.dp2.NFFG.sol3.service.model.NffgInfoEx.InvalidNffgException;

public class NffgService {
	static private ReachabilityTesterClient reachabilityTester = null;
	// The class should be thread-safe, it doesn't need to be synchronized when
	// used
	static private NffgInfoEx nffgInfo = new NffgInfoEx();

	static {
		init();
	}

	private static void init() {
		String Neo4JURL = System.getProperty("it.polito.dp2.NFFG.lab3.NEO4JURL");
		if (Neo4JURL == null) {
			Neo4JURL = "http://localhost:8080/Neo4JXML/rest";
		}
		try {
			reachabilityTester = new ReachabilityTesterClient(Neo4JURL);
		} catch (URISyntaxException e) {
			System.out.println("Could not initialize ReachabilityTesterCLient: " + e.getMessage());
		}
	}

	public static ReachabilityTesterClient getReachabilityTesterClient() throws ServiceException {
		synchronized (reachabilityTester) {
			if (reachabilityTester == null) {
				init();
			}
		}
		return reachabilityTester;
	}

	public static NffgInfo getNffgInfo() {
		return nffgInfo.getNffgInfo();
	}

	public static Nffg addNffg(NffgEx nffg) {
		return nffgInfo.addNffg(nffg).getNffg();
	}

	public static Nffg getNffg(String name) {
		try {
			return nffgInfo.getNffg(name).getNffg();
		} catch (InvalidNffgException e) {
			throw new NotFoundException();
		}
	}

	public static boolean checkAndAddPolicyName(PolicyEx policy) {
		return nffgInfo.checkAndAddPolicyName(policy);
	}

	public static String removePolicyName(String policyName) {
		return nffgInfo.removePolicyName(policyName);
	}

	public static String getNffgNameRef(String policyName) {
		String nffgName = nffgInfo.getNffgNameRef(policyName);
		if (nffgName == null) {
			throw new NotFoundException();
		}
		return nffgName;
	}

	public static NffgEx getNffgRef(String policyName) throws InvalidNffgException {
		String nffgName = getNffgNameRef(policyName);
		return nffgInfo.getNffg(nffgName);
	}

	private static PolicyEx getPolicyEx(String policyName) {
		try {
			return getNffgRef(policyName).getPolicy(policyName);
		} catch (InvalidNffgException e) {
			throw new NotFoundException();
		}
	}

	public static Policy getPolicy(String policyName) {
		PolicyEx policy = getPolicyEx(policyName);
		Policy policyResp = policy.getPolicy();
		policyResp.setNffg(policy.getNffgName());
		return policyResp;
	}

	public static Policy addPolicy(PolicyEx policy)
			throws DuplicatePolicyException, InvalidPolicyException, InvalidNffgException {
		return nffgInfo.getNffg(policy.getNffgName()).addPolicy(policy).getPolicy();
	}

	public static Policy deletePolicy(String policyName) {
		try {
			return getNffgRef(policyName).deletePolicy(policyName).getPolicy();
		} catch (InvalidNffgException e) {
			throw new NotFoundException();
		}
	}

	public static Policy setPolicy(String policyName, PolicyEx policy) throws InvalidPolicyException {
		try {
			NffgEx nffg = getNffgRef(policy.getPolicy().getName());
			if (!policy.getPolicy().getName().equals(policyName))
				throw new BadRequestException("Policy name change is unsupported.");
			// TODO handle also a NFFG change, or bad request?
			if (!nffg.getName().equals(policy.getNffgName()))
				throw new BadRequestException("NFFG name change is unsupported.");
			return nffg.setPolicy(policy).getPolicy();
		} catch (InvalidNffgException e) {
			throw new NotFoundException();
		}
	}

	public static void verifyPolicies(List<PolicyRef> list) {
		List<PolicyEx> toVerify = new ArrayList<>();
		// First check if every policy exists
		for (PolicyRef policy : list) {
			try {
				toVerify.add(getPolicyEx(policy.getName()));
			} catch (NotFoundException e) {
				throw new WebApplicationException(Response.status(Status.BAD_REQUEST)
						.entity("Could not find the policy \"" + policy.getName() + "\" for the verification request.")
						.build());
			}
		}
		// Now verify them all
		// NOTE: This only verifies the reachability policy part and ignores any
		// traversability checks
		toVerify.parallelStream().forEach(p -> p.verify());
	}
}
