package it.polito.dp2.NFFG.sol3.service.model;

import java.util.GregorianCalendar;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import it.polito.dp2.NFFG.lab2.NoGraphException;
import it.polito.dp2.NFFG.lab2.ServiceException;
import it.polito.dp2.NFFG.lab2.UnknownNameException;
import it.polito.dp2.NFFG.sol3.jaxb.Policy;
import it.polito.dp2.NFFG.sol3.jaxb.Verification;
import it.polito.dp2.NFFG.sol3.jaxb.Verification.Result;

public class PolicyEx {
	private final String nffgName;

	private Policy policy = new Policy();

	public PolicyEx(Policy policy, String nffgName) {
		assert nffgName != null;
		policy.setNffg(null);
		// Normalize optional verification to UTC
		Verification verification = policy.getVerification();
		if (verification != null) {
			XMLGregorianCalendar cal = verification.getLastVerification().normalize();
			verification.setLastVerification(cal);
		}
		this.policy = policy;
		this.nffgName = nffgName;
	}

	protected synchronized Policy getPolicy() {
		return policy;
	}

	protected String getNffgName() {
		return nffgName;
	}

	public synchronized PolicyEx verify() {
		try {
			boolean reachable = NffgService.getReachabilityTesterClient().testReachability(nffgName,
					policy.getReachability().getSource(), policy.getReachability().getDestination());
			Verification verification = new Verification();
			Result result = new Result();
			boolean boolresult = reachable == policy.isIsPositive();
			result.setValue(boolresult);
			verification.setResult(result);
			verification.setMsg("Policy verification result " + boolresult);
			GregorianCalendar gregorianCalendar = new GregorianCalendar();
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar).normalize();
			verification.setLastVerification(now);
			policy.setVerification(verification);
			return this;
		} catch (UnknownNameException | ServiceException | NoGraphException | DatatypeConfigurationException e) {
			throw new WebApplicationException(e, Response.status(Status.INTERNAL_SERVER_ERROR)
					.type(MediaType.TEXT_PLAIN_TYPE).entity(e.getMessage()).build());
		}
	}

}
