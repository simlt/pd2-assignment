package it.polito.dp2.NFFG.sol3.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;

import it.polito.dp2.NFFG.sol3.client.ws.Nffg;
import it.polito.dp2.NFFG.sol3.client.ws.NffgInfo;
import it.polito.dp2.NFFG.sol3.client.ws.Policy;

public class NffgServiceTests {
	private static Client client;
	private static final String BASE_URI = "http://localhost:8080/NffgService/rest/";
	// private static UriBuilder uriBuilder;

	@BeforeClass
	static public void createClient() {
		// Create client under test
		// uriBuilder = UriBuilder.fromUri(BASE_URI);
		client = new Client();
	}

	private Nffg loadNffg(String XML, Status expectedStatus) {
		URI uri = UriBuilder.fromUri(BASE_URI).path("nffgs").build();
		WebResource resource = client.resource(uri);
		ClientResponse response = resource.accept(MediaType.APPLICATION_XML).type(MediaType.APPLICATION_XML_TYPE)
				.entity(XML).post(ClientResponse.class);
		assertEquals("Unsuccessful nffg POST, wrong status code", expectedStatus.getStatusCode(), response.getStatus());
		if (response.getStatus() < 300)
			return response.getEntity(Nffg.class);
		else
			return null;
	}

	private Nffg getNffg(String name, Status expectedStatus) {
		URI uri = UriBuilder.fromUri(BASE_URI).path("nffgs/{name}").build(name);
		WebResource resource = client.resource(uri);
		resource.accept(MediaType.APPLICATION_XML);
		ClientResponse response = resource.get(ClientResponse.class);
		assertEquals("Unsuccessful nffg GET, wrong status code", expectedStatus.getStatusCode(), response.getStatus());
		// assertEquals(, response.getType());
		if (response.getStatus() < 300)
			return response.getEntity(Nffg.class);
		else
			return null;
	}

	private Policy loadPolicy(String XML, Status expectedStatus) {
		URI uri = UriBuilder.fromUri(BASE_URI).path("policies").build();
		WebResource resource = client.resource(uri);
		ClientResponse response = resource.accept(MediaType.APPLICATION_XML).type(MediaType.APPLICATION_XML_TYPE)
				.entity(XML).post(ClientResponse.class);
		assertEquals("Unsuccessful policy POST, wrong status code", expectedStatus.getStatusCode(),
				response.getStatus());
		if (response.getStatus() < 300)
			return response.getEntity(Policy.class);
		else
			return null;
	}

	private Policy getPolicy(String name, Status expectedStatus) {
		URI uri = UriBuilder.fromUri(BASE_URI).path("policies/{name}").build(name);
		WebResource resource = client.resource(uri);
		resource.accept(MediaType.APPLICATION_XML);
		ClientResponse response = resource.get(ClientResponse.class);
		assertEquals("Unsuccessful policy GET, wrong status code", expectedStatus.getStatusCode(),
				response.getStatus());
		if (response.getStatus() < 300)
			return response.getEntity(Policy.class);
		else
			return null;
	}

	private Policy putPolicy(String name, String XML, Status expectedStatus) {
		URI uri = UriBuilder.fromUri(BASE_URI).path("policies/{name}").build(name);
		WebResource resource = client.resource(uri);
		ClientResponse response = resource.accept(MediaType.APPLICATION_XML).type(MediaType.APPLICATION_XML).entity(XML)
				.put(ClientResponse.class);
		assertEquals("Unsuccessful policy PUT, wrong status code", expectedStatus.getStatusCode(),
				response.getStatus());
		if (response.getStatus() < 300)
			return response.getEntity(Policy.class);
		else
			return null;
	}

	private Policy deletePolicy(String name, Status expectedStatus) {
		URI uri = UriBuilder.fromUri(BASE_URI).path("policies/{name}").build(name);
		WebResource resource = client.resource(uri);
		resource.accept(MediaType.APPLICATION_XML);
		ClientResponse response = resource.delete(ClientResponse.class);
		assertEquals("Unsuccessful policy DELETE, wrong status code", expectedStatus.getStatusCode(),
				response.getStatus());
		if (response.getStatus() < 300)
			return response.getEntity(Policy.class);
		else
			return null;
	}

	private void verifyPolicies(String XML, Status expectedStatus) {
		URI uri = UriBuilder.fromUri(BASE_URI).path("verification").build();
		WebResource resource = client.resource(uri);
		ClientResponse response = resource.accept(MediaType.APPLICATION_XML).type(MediaType.APPLICATION_XML).entity(XML)
				.put(ClientResponse.class);
		assertEquals("Unsuccessful policy verification request, wrong status code", expectedStatus.getStatusCode(),
				response.getStatus());
	}

	@Test
	public void sortedTest() {
		getEmptyNffgInfo();
		getNotExistingNffg();
		loadNffg1();
		loadNffg2();
		getNffgs();
		loadDuplicateNameNffg();
		loadNffgWithInvalidLink();
		loadPolicy1();
		getPolicy1();
		putNotExistingPolicy();
		loadPolicy2();
		modifyPolicyDifferentNffg();
		putPolicy2();
		putPolicyInvalidNode();
		loadPolicy3();
		putPolicy3();
		loadDuplicateNamePolicy();
		loadPolicyInvalidNode();
		deletePolicy3();
		sendVerificationRequest();
	}

	public void getEmptyNffgInfo() {
		URI uri = UriBuilder.fromUri(BASE_URI).path("nffgInfo").build();
		WebResource resource = client.resource(uri);
		resource.accept(MediaType.APPLICATION_XML);
		ClientResponse response = resource.get(ClientResponse.class);
		assertEquals("Unsuccessful nffgInfo GET, wrong status code", Status.OK.getStatusCode(), response.getStatus());
		NffgInfo nffgInfo = response.getEntity(NffgInfo.class);
		assertNotNull(nffgInfo);
		assertEquals("nffgInfo not empty", 0, nffgInfo.getNffg().size());
	}

	public void getNotExistingNffg() {
		getNffg("nffg1", Status.NOT_FOUND);
	}

	public final void loadNffg1() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><nffgRequest name=\"nffg1\">  <node name=\"node1\" type=\"WEB_CLIENT\">    <link name=\"link1\" destination=\"node2\" />  </node>  <node name=\"node2\" type=\"FW\">    <link name=\"link2\" destination=\"node1\" />  </node></nffgRequest>";
		Nffg nffg1 = loadNffg(XML, Status.CREATED);
		assertEquals("Wrong name", "nffg1", nffg1.getName());
		assertEquals("Wrong node count", 2, nffg1.getNode().size());
	}

	public void loadNffg2() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><nffgRequest name=\"nffg2\">  <node name=\"node1\" type=\"WEB_CLIENT\">    <link name=\"link1\" destination=\"node2\" />  </node>  <node name=\"node2\" type=\"FW\" /></nffgRequest>";
		Nffg nffg2 = loadNffg(XML, Status.CREATED);
		assertEquals("Wrong name", "nffg2", nffg2.getName());
		assertEquals("Wrong node count", 2, nffg2.getNode().size());
	}

	public void getNffgs() {
		Nffg nffg1 = getNffg("nffg1", Status.OK);
		assertEquals("Wrong name", "nffg1", nffg1.getName());
		assertEquals("Wrong node count", 2, nffg1.getNode().size());

		Nffg nffg2 = getNffg("nffg2", Status.OK);
		assertEquals("Wrong name", "nffg2", nffg2.getName());
		assertEquals("Wrong node count", 2, nffg2.getNode().size());
	}

	public void loadDuplicateNameNffg() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><nffgRequest name=\"nffg2\">  <node name=\"node3\" type=\"WEB_CLIENT\">    <link name=\"link1\" destination=\"node4\" />  </node>  <node name=\"node4\" type=\"FW\" /></nffgRequest>";
		loadNffg(XML, Status.CONFLICT);
	}

	public void loadNffgWithInvalidLink() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><nffgRequest name=\"nffg3\">  <node name=\"node1\" type=\"WEB_CLIENT\">    <link name=\"link1\" destination=\"nodeInvalid\" />  </node>  <node name=\"node2\" type=\"FW\" /></nffgRequest>";
		loadNffg(XML, Status.BAD_REQUEST);
	}

	public void getNotExistingPolicy() {
		getPolicy("policy1", Status.NOT_FOUND);
	}

	public void loadPolicy1() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy1\" nffg=\"nffg1\" isPositive=\"true\">  <reachability source=\"node2\" destination=\"node1\" />  <verification>    <result value=\"true\" />    <msg>OK</msg>    <lastVerification>2016-10-18T18:00:00Z</lastVerification>  </verification></policy>";
		Policy policy1 = loadPolicy(XML, Status.CREATED);
		assertEquals("Wrong name", "policy1", policy1.getName());
		// assertEquals("Wrong nffg", "nffg1", policy1.getNffg());
		assertNotNull(policy1.getReachability());
		assertEquals(0, policy1.getTraversal().size());
		assertNotNull(policy1.getVerification());
	}

	public void getPolicy1() {
		Policy policy1 = getPolicy("policy1", Status.OK);
		assertEquals("Wrong name", "policy1", policy1.getName());
		// assertEquals("Wrong nffg", "nffg1", policy1.getNffg());
		assertNotNull(policy1.getReachability());
		assertEquals(0, policy1.getTraversal().size());
		assertNotNull(policy1.getVerification());
	}

	public void putNotExistingPolicy() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy2\" nffg=\"nffg1\" isPositive=\"true\">  <reachability source=\"node2\" destination=\"node1\" /></policy>";
		putPolicy("policy2", XML, Status.NOT_FOUND);
	}

	public void loadPolicy2() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy2\" nffg=\"nffg1\" isPositive=\"true\">  <reachability source=\"node2\" destination=\"node1\" />  <traversal type=\"FW\" /></policy>";
		Policy policy2 = loadPolicy(XML, Status.CREATED);
		assertEquals("Wrong name", "policy2", policy2.getName());
		// assertEquals("Wrong nffg", "nffg1", policy2.getNffg());
		assertNotNull(policy2.getReachability());
		assertNotNull(policy2.getTraversal());
		assertEquals(null, policy2.getVerification());
	}

	public void modifyPolicyDifferentNffg() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy2\" nffg=\"nffg2\" isPositive=\"true\">  <reachability source=\"node2\" destination=\"node1\" /></policy>";
		putPolicy("policy2", XML, Status.BAD_REQUEST);
	}

	public void putPolicy2() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy2\" isPositive=\"false\">  <reachability source=\"node2\" destination=\"node1\" /></policy>";
		putPolicy("policy2", XML, Status.OK);
	}

	public void putPolicyInvalidNode() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy2\" isPositive=\"false\">  <reachability source=\"nodeInvalid\" destination=\"node2\" /></policy>";
		putPolicy("policy2", XML, Status.BAD_REQUEST);
	}

	public void loadPolicy3() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy3\" nffg=\"nffg2\" isPositive=\"true\">  <reachability source=\"node1\" destination=\"node2\" /></policy>";
		loadPolicy(XML, Status.CREATED);
	}

	public void putPolicy3() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy3\" isPositive=\"true\">  <reachability source=\"node2\" destination=\"node1\" /></policy>";
		putPolicy("policy3", XML, Status.OK);
	}

	public void loadDuplicateNamePolicy() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy3\" nffg=\"nffg1\" isPositive=\"true\">  <reachability source=\"node2\" destination=\"node1\" /></policy>";
		loadPolicy(XML, Status.CONFLICT);
	}

	public void loadPolicyInvalidNode() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy4\" nffg=\"nffg2\" isPositive=\"true\">  <reachability source=\"nodeNotValid\" destination=\"nodeNotValid2\" /></policy>";
		loadPolicy(XML, Status.BAD_REQUEST);
	}

	public void deletePolicy3() {
		deletePolicy("policy3", Status.OK);
		// restore policy3
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><policy name=\"policy3\" nffg=\"nffg2\" isPositive=\"true\">  <reachability source=\"node2\" destination=\"node1\" /></policy>";
		loadPolicy(XML, Status.CREATED);
	}

	public void sendVerificationRequest() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><verificationRequest>  <policyRef name=\"policy1\" />  <policyRef name=\"policy2\" /></verificationRequest>";
		verifyPolicies(XML, Status.OK);

		Policy policy1 = getPolicy("policy1", Status.OK);
		assertNotNull("No verification found after verification request", policy1.getVerification());
		assertNotNull("No result found in verification", policy1.getVerification().getResult());
		assertEquals("Wrong Verification result", true, policy1.getVerification().getResult().isValue());

		Policy policy2 = getPolicy("policy2", Status.OK);
		assertNotNull("No verification found after verification request", policy2.getVerification());
		assertNotNull("No result found in verification", policy2.getVerification().getResult());
		assertEquals("Wrong Verification result", false, policy2.getVerification().getResult().isValue());
	}
	
	public void sendVerificationRequestNotExisting() {
		String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><verificationRequest>  <policyRef name=\"policy4\" /> </verificationRequest>";
		verifyPolicies(XML, Status.BAD_REQUEST);
	}
}